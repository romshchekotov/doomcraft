@file:Suppress("UnstableApiUsage", "VulnerableLibrariesLocal")
// Bad Practice! Vulnerable library included! DO NOT USE!

import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import net.minecrell.pluginyml.bukkit.BukkitPluginDescription.PluginLoadOrder.STARTUP
import org.ajoberstar.grgit.Grgit
import org.ajoberstar.grgit.Person
import java.security.MessageDigest

plugins {
    id("java")
    id("xyz.jpenilla.run-paper") version "1.0.6"
    id("com.github.johnrengelman.shadow") version "7.1.0"
    id("io.papermc.paperweight.userdev") version "1.3.6"
    id("net.minecrell.plugin-yml.bukkit") version "0.5.2"
    id("org.checkerframework") version "0.6.20"
    id("org.ajoberstar.grgit") version "5.0.0"
}

group = "org.doomworks"
version = "0.0.1"

dependencies {
    /* Paper & Minecraft Dependencies */
    paperweightDevelopmentBundle("io.papermc.paper:dev-bundle:1.19.2-R0.1-SNAPSHOT")
    compileOnly("io.papermc.paper:paper-api:1.19.2-R0.1-SNAPSHOT")
    implementation("io.papermc:paperlib:1.0.7")

    /* Java & Utility Dependencies */
    compileOnly("org.projectlombok:lombok:1.18.24")
    annotationProcessor("org.projectlombok:lombok:1.18.24")
    implementation("org.reflections:reflections:0.10.2")

    /* Persistence */
    implementation("org.xerial:sqlite-jdbc:3.40.0.0")
    implementation("com.j256.ormlite:ormlite-jdbc:6.1")
    implementation("javax.persistence:javax.persistence-api:2.2")

    /* Test Dependencies */
    testImplementation("org.mockito:mockito-core:4.8.0")
    testImplementation("org.mockito:mockito-junit-jupiter:4.8.0")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.0")
}

val git: Grgit = Grgit.open {
    dir = rootDir
}

val targetJavaVersion = 17
val charset: String = Charsets.UTF_8.name()

tasks {
    getByName<Test>("test") {
        useJUnitPlatform()
    }

    getByName<ShadowJar>("shadowJar") {
        relocate("io.papermc.lib", "org.doomworks.doomcraft.paperlib")
    }

    getByName<ProcessResources>("processResources") {
        val props = "version" to version
        inputs.properties(mapOf(props))
        filteringCharset = charset
        filesMatching("plugin.yml") {
            expand(props)
        }
        dependsOn("genTexturesSHA1", "archiveTextures")
    }

    getByName<Javadoc>("javadoc") {
        options {
            this as StandardJavadocDocletOptions
            encoding = charset
            addBooleanOption("-allow-script-in-comments", true)
            addStringOption("sourcetab", "2")
            header = """
                <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
                DoomCraft - A Minecraft plugin for PaperMC servers.
            """.replace("\n", "").trimIndent()
        }
    }

    getByName("assemble").dependsOn("reobfJar")

    withType<JavaCompile>().configureEach {
        options.encoding = charset
        if(targetJavaVersion >= 10 || JavaVersion.current().isJava10Compatible) {
            options.release.set(targetJavaVersion)
        }
    }

    register<Zip>("archiveTextures") {
        archiveFileName.set("textures.zip")
        destinationDirectory.set(file("src/main/resources"))
        from("src/main/resources/resourcepack")
        println("Archiving textures...")
    }

    register("genTexturesSHA1") {
        dependsOn("archiveTextures")
        doLast {
            val sha1 = MessageDigest.getInstance("SHA-1")
            val zip = file("src/main/resources/textures.zip")
            sha1.update(zip.readBytes())
            val hash = sha1.digest().joinToString("") { "%02x".format(it) }
            file("src/main/resources/textures.sha1").writeText(hash)
            println("Generated SHA1 hash for textures.zip: $hash")
            println("In order to update the resources, please push the new textures.zip and textures.sha1 to the repository.")
        }
    }

    register("uploadTextures") {
        dependsOn("genTexturesSHA1")
        doLast {
            val status = git.status()
            if (status.unstaged.allChanges.contains("src/main/resources/textures.zip") ||
                status.unstaged.allChanges.contains("src/main/resources/textures.sha1")
            ) {
                git.reset(
                    mapOf(
                        "commit" to "HEAD",
                        "mode" to "soft"
                    )
                )
                git.add(
                    mapOf(
                        "patterns" to setOf("src/main/resources/textures.sha1", "src/main/resources/textures.zip"),
                        "update" to false
                    )
                )
                git.commit(
                    mapOf(
                        "message" to "\uD83C\uDF71 updated textures (gradle-automated)",
                        "author" to Person("Roman Shchekotov (Doomie ft. Gradle)", "rom.shchekotov+bot@gmail.com")
                    )
                )
                git.push(
                    mapOf(
                        "remote" to "origin",
                        "refsOrSpecs" to listOf("master")
                    )
                )
            }
        }
    }

    named("build").get().finalizedBy("uploadTextures")
}

bukkit {
    load = STARTUP
    main = "org.doomworks.doomcraft.DoomCraft"
    apiVersion = "1.19"
    authors = listOf("Doomie")
    description = "A plugin that adds more depth to Minecraft."
    prefix = "doomcraft"
}

java {
    val javaVersion = JavaVersion.toVersion(targetJavaVersion)
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
    if(JavaVersion.current() < javaVersion) {
        toolchain.languageVersion.set(JavaLanguageVersion.of(targetJavaVersion))
    }
}

apply(plugin = "org.checkerframework")