package org.doomworks.doomcraft.story.data;

import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.doomworks.doomcraft.story.Storyteller;

@Data
public class Story {
    private final String title;
    private final StoryElement origin;
    private Storyteller storyteller;

    public void start(Player... players) {
        for (Player player : players) {
            String command = "title %s title {\"text\":\"%s\"}".formatted(player.getName(), title);
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
        }
    }

    public void playScene(Player... players) {
        StoryElement next = origin.play(players);
        while (next != null) {
            next = next.play(players);
        }
    }
}
