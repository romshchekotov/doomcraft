package org.doomworks.doomcraft.story.data;

import lombok.Data;
import org.bukkit.entity.Player;

@Data
public abstract class StoryElement {
    StoryElement[] next;
    private String name;

    public abstract StoryElement play(Player... players);
}
