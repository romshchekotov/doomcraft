package org.doomworks.doomcraft.story;

import net.kyori.adventure.text.Component;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import org.doomworks.doomcraft.DoomCraft;
import org.doomworks.doomcraft.story.data.Story;

import java.util.Collection;
import java.util.function.BiConsumer;

/*
 *  TODO: Animated Villager
 *  https://www.youtube.com/watch?v=R6l0thbLN48
 */

public class Storyteller implements Listener {
    Entity entity;
    Story story;
    boolean watchingPlayer;

    public Storyteller(Component name, Location location, BiConsumer<World, Location> environment, Story story) {
        Villager villager = location.getWorld().spawn(location, Villager.class);
        this.entity = villager;
        this.story = story;
        story.setStoryteller(this);
        environment.accept(location.getWorld(), location);
        villager.customName(name);
        villager.setInvulnerable(true);
        villager.setAI(false);
        villager.setSilent(true);
        villager.setCollidable(false);
        villager.setGravity(false);

        Collection<Player> players = location.getWorld().getNearbyPlayers(location, 64);
        if (!players.isEmpty()) {
            Player[] playerArray = players.toArray(new Player[0]);
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (watchingPlayer) {
                        villager.lookAt(playerArray[0]);
                    }
                }
            }.runTaskTimer(DoomCraft.getInstance(), 0, 20);
            story.start(playerArray);
        }
    }
}
