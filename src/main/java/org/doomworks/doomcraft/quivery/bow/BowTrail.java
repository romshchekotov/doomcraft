package org.doomworks.doomcraft.quivery.bow;

import io.papermc.paper.event.player.PlayerStopUsingItemEvent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.HashMap;
import java.util.Map;

public class BowTrail implements Listener {
    static Map<Player, Integer> trails = new HashMap<>();

    @EventHandler
    public void onBowSpan(PlayerInteractEvent event) {
        Material item = event.getPlayer().getInventory().getItemInMainHand().getType();
        boolean rightClick = event.getAction() == Action.RIGHT_CLICK_AIR ||
                event.getAction() == Action.RIGHT_CLICK_BLOCK;
        if (item == Material.BOW && rightClick) {
            trails.put(event.getPlayer(), Bukkit.getServer().getCurrentTick());
            // TODO: Do Math and Compute Trajectory
        }
    }

    @EventHandler
    public void onBowShot(PlayerStopUsingItemEvent event) {
        trails.remove(event.getPlayer());
    }
}
