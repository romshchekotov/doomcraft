package org.doomworks.doomcraft.quivery;

import org.bukkit.Bukkit;
import org.doomworks.doomcraft.DoomCraft;
import org.doomworks.doomcraft.quivery.arrow.ArrowHandler;
import org.doomworks.doomcraft.quivery.bow.BowTrail;

public class QuiveryCore {
    static boolean registered = false;
    public static void register() {
        if(!registered) {
            Bukkit.getLogger().info("Registering QuiveryCore...");
            Bukkit.getPluginManager().registerEvents(new ArrowHandler(), DoomCraft.getInstance());
            Bukkit.getPluginManager().registerEvents(new BowTrail(), DoomCraft.getInstance());
            registered = true;
        }
    }
}
