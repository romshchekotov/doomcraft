package org.doomworks.doomcraft.quivery.arrow;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.util.Vector;

import java.util.Map;

@SuppressWarnings("unused")
public class ExplosiveArrow extends DoomArrow {
    public ExplosiveArrow() {
        super(DoomArrow.EXPLOSIVE_ARROW, Particle.CLOUD);
    }

    @Override
    public ShapedRecipe getRecipe() {
        String[] shape = {" T ", " I ", " F "};
        Map<Character, Material> ingredients = Map.of(
                'T', Material.TNT,
                'I', Material.STICK,
                'F', Material.FEATHER
        );
        Component name = Component.text("Explosive Arrow")
                .color(TextColor.color(0x880000));
        return super.createArrow("explosive_arrow", name, shape, ingredients);
    }

    @Override
    public void activate(ProjectileHitEvent event) {
        event.getEntity().remove();
        event.setCancelled(true);
        Location location = null;
        if (event.getHitEntity() != null) {
            location = event.getHitEntity().getLocation();
        } else if (event.getHitBlock() != null) {
            location = event.getHitBlock().getLocation();
            location.add(new Vector(0, 1, 0));
        }

        if (location != null) {
            TNTPrimed tnt = event.getEntity().getWorld().spawn(location, TNTPrimed.class);
            tnt.setFuseTicks(0);
        }
    }
}
