package org.doomworks.doomcraft.quivery.arrow;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Particle;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.checkerframework.common.value.qual.ArrayLen;
import org.doomworks.doomcraft.DoomCraft;

import java.util.Map;

@AllArgsConstructor
public abstract class DoomArrow implements Listener {
    private static int nextID = 1048576;
    public static final int IMMOBILIZATION_ARROW = ID();
    public static final int WARP_ARROW = ID();
    public static final int EXPLOSIVE_ARROW = ID();
    public static final int GRAPPLING_ARROW = ID();
    @Getter
    private final int ID;
    @Getter
    private final Particle trail;

    public static int ID() {
        return nextID++;
    }

    public ShapedRecipe createArrow(String ref, Component displayName, String @ArrayLen(3) [] shape, Map<Character, Material> ingredients) {
        ItemStack item = new ItemStack(Material.ARROW);
        ItemMeta meta = item.getItemMeta();
        meta.setCustomModelData(getID());
        meta.displayName(displayName);
        item.setItemMeta(meta);
        NamespacedKey key = new NamespacedKey(DoomCraft.getInstance(), ref);
        ShapedRecipe recipe = new ShapedRecipe(key, item);
        recipe.shape(shape[0], shape[1], shape[2]);
        ingredients.forEach(recipe::setIngredient);
        return recipe;
    }

    public abstract void activate(ProjectileHitEvent event);
    public abstract ShapedRecipe getRecipe();
}
