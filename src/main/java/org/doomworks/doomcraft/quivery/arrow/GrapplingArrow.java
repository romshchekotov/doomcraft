package org.doomworks.doomcraft.quivery.arrow;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Material;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ShapedRecipe;

import java.util.Map;

@SuppressWarnings("unused")
public class GrapplingArrow extends DoomArrow {
    public GrapplingArrow() {
        super(GRAPPLING_ARROW, null);
    }

    @Override
    public void activate(ProjectileHitEvent event) {

    }

    @Override
    public ShapedRecipe getRecipe() {
        String[] shape = {" H ", " I ", " F "};
        Map<Character, Material> ingredients = Map.of(
                'H', Material.TRIPWIRE_HOOK,
                'I', Material.STICK,
                'F', Material.FEATHER
        );
        Component name = Component.text("Grappling Arrow")
                .color(TextColor.color(0x949494));
        return super.createArrow("grappling_arrow", name, shape, ingredients);
    }
}
