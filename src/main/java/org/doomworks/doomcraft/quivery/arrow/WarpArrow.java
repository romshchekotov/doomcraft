package org.doomworks.doomcraft.quivery.arrow;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.util.Vector;

import java.util.Map;

@SuppressWarnings("unused")
public class WarpArrow extends DoomArrow {
    public WarpArrow() {
        super(DoomArrow.WARP_ARROW, Particle.SPELL_WITCH);
    }

    @Override
    public ShapedRecipe getRecipe() {
        String[] shape = {" E ", " I ", " F "};
        Map<Character, Material> ingredients = Map.of(
                'E', Material.ENDER_PEARL,
                'I', Material.STICK,
                'F', Material.FEATHER
        );
        Component name = Component.text("Warp Arrow")
                .color(TextColor.color(0x9641e5));
        return super.createArrow("warp_arrow", name, shape, ingredients);
    }

    @Override
    public void activate(ProjectileHitEvent event) {
        if(event.getEntity().getShooter() instanceof Player player) {
            if(event.getHitEntity() instanceof LivingEntity entity) {
                Vector target = player.getLocation().getDirection().multiply(3);
                Location location = player.getLocation().add(target);
                if(player.getWorld().getBlockAt(location).getType() == Material.AIR) {
                    event.setCancelled(true);
                    entity.teleport(location);
                }
            } else if(event.getHitBlock() != null) {
                Location location = event.getHitBlock().getLocation();
                if(event.getHitBlockFace() == BlockFace.UP) {
                    player.teleport(location.add(0, 1, 0));
                }
            }
        }
    }
}
