package org.doomworks.doomcraft.quivery.arrow;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Map;

import static org.doomworks.doomcraft.util.MinecraftUtil.seconds;

@SuppressWarnings("unused")
public class ImmobilizationArrow extends DoomArrow {
    public ImmobilizationArrow() {
        super(DoomArrow.IMMOBILIZATION_ARROW, Particle.ELECTRIC_SPARK);
    }

    @Override
    public ShapedRecipe getRecipe() {
        String[] shape = {"SFS", " I ", " E "};
        Map<Character, Material> ingredients = Map.of(
                'S', Material.STRING,
                'F', Material.FLINT,
                'I', Material.STICK,
                'E', Material.FEATHER
        );
        Component name = Component.text("Immobilization Arrow")
                .color(TextColor.color(0xd3d3d3));
        return super.createArrow("immobilization_arrow", name, shape, ingredients);
    }

    @Override
    public void activate(ProjectileHitEvent event) {
        if(event.getHitEntity() instanceof LivingEntity entity) {
            entity.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, seconds(20), 64));
            entity.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, seconds(20), 128));
            entity.getWorld().getBlockAt(entity.getLocation()).setType(Material.COBWEB);
        } else if(event.getHitBlock() != null) {
            Location location = event.getHitBlock().getLocation();
            location.add(0, 1, 0);
            Block trail = event.getHitBlock().getWorld().getBlockAt(location);
            if(trail.getType() == Material.AIR) {
                trail.setType(Material.COBWEB);
            }
        }
    }
}
