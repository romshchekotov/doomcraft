package org.doomworks.doomcraft.quivery.arrow;

import io.papermc.paper.event.player.PlayerInventorySlotChangeEvent;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.doomworks.doomcraft.DoomCraft;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class ArrowHandler implements Listener {
    private static final Map<Player, Set<Projectile>> projectiles = new HashMap<>();
    private static final Map<Integer, DoomArrow> arrows = new HashMap<>();
    private static final Map<Location, DoomArrow> arrowLocations = new LinkedHashMap<>();
    private static final Map<Projectile, Bat> grapplingHooks = new HashMap<>();

    public ArrowHandler() {
        Set<Class<? extends DoomArrow>> arrowTypes = new Reflections(getClass().getPackageName())
                .getSubTypesOf(DoomArrow.class);
        for (Class<?> clazz : arrowTypes) {
            try {
                DoomArrow arrow = (DoomArrow) clazz.getConstructor().newInstance();
                registerArrow(arrow);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                     NoSuchMethodException ignored) {
                Bukkit.getLogger().warning("Failed to register arrow: " + clazz.getSimpleName());
            }
        }
    }

    public static void registerArrow(DoomArrow arrow) {
        if(!arrows.containsKey(arrow.getID())) {
            arrows.put(arrow.getID(), arrow);
            ShapedRecipe recipe = arrow.getRecipe();
            Bukkit.addRecipe(recipe);
            Component comp = recipe.getResult().getItemMeta().displayName();
            if (comp != null) {
                String name = PlainTextComponentSerializer.plainText().serialize(comp);
                Bukkit.getLogger().info("Registered arrow: '" + name + "'");
            } else {
                Bukkit.getLogger().info("Registered arrow: <" + arrow.getID() + ">");
            }
        }
    }

    public static Set<Projectile> getProjectiles(Player player) {
        if(!projectiles.containsKey(player)) {
            projectiles.put(player, new HashSet<>());
        }
        return projectiles.get(player);
    }

    public static void createProjectile(Player player, Projectile projectile) {
        getProjectiles(player).add(projectile);
        animateTrail(projectile);
    }

    public static Projectile processProjectile(Player player, Projectile projectile) {
        return getProjectiles(player).remove(projectile) ? projectile : null;
    }

    @EventHandler
    public static void onProjectileCollect(PlayerInventorySlotChangeEvent event) {
        ItemStack stack = event.getNewItemStack();
        ItemStack old = event.getOldItemStack();
        if (stack.getType() == old.getType()) {
            stack.subtract(old.getAmount());
        }
        Location location = event.getPlayer().getLocation();

        /* Garbage Collection */
        if (arrowLocations.size() > 256) {
            Iterator<Location> iterator = arrowLocations.keySet().iterator();
            for (int i = 0; i < 128; i++) {
                iterator.next();
                iterator.remove();
            }
        }

        /* Finding the closest arrow */
        if (stack.getType().equals(Material.ARROW)) {
            Location closest = null;
            double bestDistance = Double.MAX_VALUE;
            for (Map.Entry<Location, DoomArrow> entry : arrowLocations.entrySet()) {
                double newDistance = entry.getKey().distance(location);
                if (newDistance < 3 && (closest == null || newDistance < bestDistance)) {
                    closest = entry.getKey();
                    bestDistance = newDistance;
                }
            }

            if (closest != null) {
                DoomArrow arrow = arrowLocations.get(closest);
                arrowLocations.remove(closest);
                ItemStack replacement = arrow.getRecipe().getResult();
                replacement.setAmount(stack.getAmount());
                event.getPlayer().getInventory().remove(event.getNewItemStack());
                event.getPlayer().getInventory().addItem(event.getOldItemStack(), replacement);
            }
        }
    }

    @EventHandler
    public static void onDischarge(EntityShootBowEvent event) {
        ItemStack item = event.getConsumable();
        if (item == null) return;
        ItemMeta meta = item.getItemMeta();
        if (!meta.hasCustomModelData()) return;
        if (event.getEntity() instanceof Player player) {
            if (event.getProjectile() instanceof Projectile projectile) {
                createProjectile(player, projectile);
                // Check if arrow is a GrapplingArrow
                if (meta.getCustomModelData() == DoomArrow.GRAPPLING_ARROW) {
                    Bat bat = player.getWorld().spawn(player.getLocation(), Bat.class);
                    bat.setAI(false);
                    bat.setInvulnerable(true);
                    bat.setSilent(true);
                    bat.setMetadata("grapplingArrow", new FixedMetadataValue(DoomCraft.getInstance(), projectile));
                    bat.setLeashHolder(projectile);
                    bat.addPassenger(player);
                    grapplingHooks.put(projectile, bat);
                }
                projectile.setMetadata("doomcraft:arrow",
                        new FixedMetadataValue(DoomCraft.getInstance(), meta.getCustomModelData()));
            }
        }
    }

    @EventHandler
    public static void onJoin(PlayerJoinEvent event) {

    }

    @EventHandler
    public static void onInventorySlotFocusChange(io.papermc.paper.event.player.PlayerStopUsingItemEvent event) {

    }

    public static void animateTrail(Projectile projectile) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!projectile.isValid() || projectile.isDead()) {
                    cancel();
                    return;
                }

                Particle particle = Particle.SPELL_INSTANT;
                int count = 5;
                if (projectile.hasMetadata("doomcraft:arrow")) {
                    int id = projectile.getMetadata("doomcraft:arrow").get(0).asInt();
                    if (arrows.containsKey(id)) {
                        particle = Optional.ofNullable(arrows.get(id).getTrail()).orElse(Particle.SPELL_INSTANT);
                        if (id == DoomArrow.IMMOBILIZATION_ARROW) {
                            count = 100;
                        }
                    }
                }

                Location location = projectile.getLocation();
                if (projectile.isOnGround()) {
                    location = projectile.getLocation().add(0, 0.5, 0);
                    location.getWorld().spawnParticle(particle, location, count, 0.1, 2, 0.1, 0.1);
                } else {
                    location.getWorld().spawnParticle(particle, location, count, 0.1, 0.1, 0.1, 0.1);
                }
            }
        }.runTaskTimer(DoomCraft.getInstance(), 0, 1);
    }

    @EventHandler
    public static void onImpact(ProjectileHitEvent event) {
        Projectile projectile = event.getEntity();
        if (projectile.getShooter() instanceof Player player) {
            Projectile processed = processProjectile(player, projectile);
            if (processed != null) {
                if (processed.hasMetadata("doomcraft:arrow")) {
                    MetadataValue metaValue = processed.getMetadata("doomcraft:arrow").get(0);
                    int value = metaValue.asInt();
                    DoomArrow arrow = arrows.getOrDefault(value, null);
                    if (arrow != null) {
                        arrow.activate(event);
                        if (!event.isCancelled()) {
                            boolean stuckInBlock = event.getHitBlock() != null;
                            boolean stillExists = event.getEntity().isValid();
                            if (stuckInBlock && stillExists) {
                                arrowLocations.put(event.getHitBlock().getLocation(), arrow);
                            }
                        }
                        if (grapplingHooks.containsKey(projectile)) {
                            grapplingHooks.get(projectile).remove();
                            grapplingHooks.remove(projectile);
                        }
                    }
                } else {
                    Bukkit.getLogger().warning("Recorded Projectile has no metadata!");
                }
            }
        }
    }
}
