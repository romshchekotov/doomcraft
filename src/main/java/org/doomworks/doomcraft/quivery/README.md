# DoomCraft - Quivery Expansion
## ~~Goal 1: Implement custom 'Immobilization-Arrow' texture~~
## ~~Goal 2: Give Arrow's IDs.~~
## ~~Goal 3: Implement custom crafting recipe for the 'Immobilization-Arrow'.~~
## ~~Goal 4: Implement the 'Immobilization-Arrow' effect.~~
## Goal 5: Add 'projectile-trajectory'-prediction with smoke-particles to the bow 'On-Draw'
## Goal 6: Create a custom crafting recipe for the 'Bow with Visor'