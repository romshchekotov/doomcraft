package org.doomworks.doomcraft.util;

public class MinecraftUtil {
    private MinecraftUtil() {}

    public static int seconds(int seconds) {
        return seconds * 20;
    }
}
