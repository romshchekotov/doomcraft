package org.doomworks.doomcraft.util;

import lombok.Builder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;
import org.doomworks.doomcraft.DoomCraft;

import java.util.LinkedHashMap;

public class BuildUtil {
    World world;
    Location origin;
    Location current;
    boolean debug = false;

    LinkedHashMap<Location, Material> blocks;

    @Builder
    private BuildUtil(World world, Location origin) {
        this.world = world;
        this.origin = origin;
        this.current = origin;
        this.blocks = new LinkedHashMap<>();
    }

    @SuppressWarnings("UnusedReturnValue")
    public BuildUtil move(int x, int y, int z) {
        current.add(x, y, z);
        return relocate();
    }

    @SuppressWarnings("UnusedReturnValue")
    public BuildUtil block(Location location, Material material) {
        this.blocks.put(location.clone(), material);
        return this;
    }

    /**
     * Builds a line of blocks in the given direction.
     * The direction is a vector for each step to take.
     * @param material block material
     * @param length number of blocks to build
     * @param vector distance between each block
     */
    @SuppressWarnings("UnusedReturnValue")
    public BuildUtil blockLine(Material material, int length, short[] vector) {
        current = origin.clone();
        for (int i = 0; i < length; i++) {
            current.add(vector[0], vector[1], vector[2]);
            block(current, material);
        }
        return this;
    }

    public BuildUtil blockSquareCentered(Material material, int length) {
        Location original = origin.clone();
        current = origin.clone();
        int half = -(length / 2);

        move(half, 0, half);
        blockLine(material, length - 1, new short[]{1, 0, 0});
        relocate().blockLine(material, length - 1, new short[]{0, 0, 1});
        relocate().blockLine(material, length - 1, new short[]{-1, 0, 0});
        relocate().blockLine(material, length - 1, new short[]{0, 0, -1});
        origin = original;
        return this;
    }

    public BuildUtil relocate() {
        origin = current;
        return this;
    }

    public BuildUtil debug() {
        this.debug = !debug;
        return this;
    }

    public void build(int delay) {
        new BukkitRunnable() {
            int i = 0;

            @Override
            public void run() {
                if (i >= blocks.size()) {
                    cancel();
                    return;
                }
                Location location = (Location) blocks.keySet().toArray()[i];
                Material material = blocks.get(location);
                world.getBlockAt(location).setType(material);
                if (debug) {
                    DoomCraft.getInstance().getLogger().info("Built " + material + " at " + location);
                }
                i++;
            }
        }.runTaskTimer(DoomCraft.getInstance(), 0, delay);
    }
}
