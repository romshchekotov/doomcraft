package org.doomworks.doomcraft.event;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.doomworks.doomcraft.util.BuildUtil;

public class BlockPlaceHandler implements Listener {
    @EventHandler
    public void handleBlockPlace(BlockPlaceEvent event) {
        Material placed = event.getBlockPlaced().getType();

        if(placed == Material.CAKE) {
            World world = event.getPlayer().getWorld();
            Location origin = event.getBlockPlaced().getLocation();

            BuildUtil.builder().world(world).origin(origin).build().debug()
                    .blockSquareCentered(Material.CAKE, 9)
                    .build(20);
        }
    }
}
