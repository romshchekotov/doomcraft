package org.doomworks.doomcraft.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.doomworks.doomcraft.DoomConstants;
import org.doomworks.doomcraft.DoomCraft;

public class JoinHandler implements Listener {
    @EventHandler
    public void handleJoin(PlayerJoinEvent event) {
        event.getPlayer().sendMessage("Welcome to DoomCraft!");
        if(DoomConstants.TEXTURE_HASH != null) {
            event.getPlayer().setResourcePack(DoomConstants.TEXTURE_PACK, DoomConstants.TEXTURE_HASH, true);
            DoomCraft.getInstance().enableTextureDependentFeatures();
        }
    }
}
