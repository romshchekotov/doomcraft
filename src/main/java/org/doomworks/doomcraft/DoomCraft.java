package org.doomworks.doomcraft;

import lombok.Getter;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.doomworks.doomcraft.event.BlockPlaceHandler;
import org.doomworks.doomcraft.event.JoinHandler;
import org.doomworks.doomcraft.quivery.QuiveryCore;

import java.util.ArrayList;
import java.util.List;

public class DoomCraft extends JavaPlugin {
    @Getter
    private static DoomCraft instance;

    List<Listener> listeners = new ArrayList<>(List.of(
            new JoinHandler(),
            new BlockPlaceHandler()
    ));

    @Override
    public void onEnable() {
        instance = this;
        listeners.forEach(listener -> getServer().getPluginManager().registerEvents(listener, this));
    }

    @Override
    public void onDisable() {}

    public void enableTextureDependentFeatures() {
        QuiveryCore.register();
    }
}
