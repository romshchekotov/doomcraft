package org.doomworks.doomcraft;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public final class DoomConstants {
    private DoomConstants() {}

    public static final String REPOSITORY = "https://gitlab.com/romshchekotov/doomcraft";
    public static final String REPOSITORY_BRANCH = "master";
    public static final String TEXTURE_PACK = repo("src/main/resources/textures.zip");
    public static final String TEXTURE_HASH = web(repo("src/main/resources/textures.sha1"));

    /**
     * Reads the file provided by the url
     * and returns the String contents
     * @param url web url
     * @return file contents
     */
    private static String web(String url) {
        try {
            InputStream is = new URL(url).openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String repo(String path) {
        return "%s/-/raw/%s/%s".formatted(REPOSITORY, REPOSITORY_BRANCH, path);
    }
}
