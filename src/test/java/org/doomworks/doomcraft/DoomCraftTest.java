package org.doomworks.doomcraft;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DoomCraftTest {
    /**
     * Exists so the IntelliJ Pre-Commit Hook
     * triggers as it should.
     */
    @Test
    public void preCommit() {
        assertTrue(true);
    }
}